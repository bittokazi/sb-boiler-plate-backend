package com.backend.boiler.plate.Utils;

public class Mappings {
	public static final String API = "/api";
	public static final String LOGIN = "/public/login/check";
	
	//User Mappings
	public static final String API_USERS = "/users";
	public static final String API_USER_INFO = API_USERS+"/info";
	public static final String API_USER_ADD = API_USERS;
	public static final String API_USER_SINGLE = API_USERS+"/{id}";
	
	//Category Mappings
	public static final String API_CATEGORY = "/categories";
	public static final String API_CATEGORY_ADD = API_CATEGORY;
	
	//Notes Mappings
	public static final String API_NOTES = "/notes";
	public static final String API_NOTES_ADD = API_NOTES;
	public static final String API_NOTES_SINGLE = API_NOTES+"/{id}";
	
	
}
