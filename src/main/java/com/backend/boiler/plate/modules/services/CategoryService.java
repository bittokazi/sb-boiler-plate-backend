package com.backend.boiler.plate.modules.services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.boiler.plate.model.Category;
import com.backend.boiler.plate.modules.base.data.repository.CategoryEntityRepository;
import com.backend.boiler.plate.modules.base.data.repository.UserRepository;

@Service
public class CategoryService {

	@Autowired
	private CategoryEntityRepository categoryEntityRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	public Category saveCategory(Category category, String username) {
		category.setUser(userRepository.findOneByUsername(username).get());
		return categoryEntityRepository.save(category);
	}
	
	public Set<Category> getCategories(String username) {
		return userRepository.findOneByUsername(username).get().getCategories();
	}
}
