package com.backend.boiler.plate.modules.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.boiler.plate.model.Note;
import com.backend.boiler.plate.model.User;
import com.backend.boiler.plate.modules.base.data.repository.NoteEntityRepository;
import com.backend.boiler.plate.modules.base.data.repository.UserRepository;

@Service
public class NoteService {

	@Autowired
	private NoteEntityRepository noteEntityRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	public Note saveNote(Note note, String username) {
		note.setUser(userRepository.findOneByUsername(username).get());
		return noteEntityRepository.save(note);
	}
	
	public List<Note> getNotes(String username) {
		return userRepository.findOneByUsername(username).get().getNotes();
	}
	
	public Note getNote(Long id) {
		return noteEntityRepository.findById(id).get();
	}
	
	public void deleteNote(Long id) {
		noteEntityRepository.deleteById(id);
	}
}
