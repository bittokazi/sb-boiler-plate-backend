package com.backend.boiler.plate.modules.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.boiler.plate.Utils.Mappings;
import com.backend.boiler.plate.model.Note;
import com.backend.boiler.plate.modules.services.NoteService;

@RestController
@RequestMapping(Mappings.API)
public class NoteController {
	@Autowired
	private NoteService noteService;
	
	@PostMapping(Mappings.API_NOTES_ADD)
	public Note saveNote(@RequestBody Note note, HttpServletRequest request) {
		return noteService.saveNote(note, request.getUserPrincipal().getName());
	}
	
	@GetMapping(Mappings.API_NOTES)
	public List<Note> getNotes(HttpServletRequest request) {
		return noteService.getNotes(request.getUserPrincipal().getName());
	}
	
	@GetMapping(Mappings.API_NOTES_SINGLE)
	public Note getNote(@PathVariable long id, HttpServletRequest request) {
		return noteService.getNote(id);
	}
	
	@PutMapping(Mappings.API_NOTES_SINGLE)
	public Note updateNotes(@RequestBody Note note, HttpServletRequest request) {
		return noteService.saveNote(note, request.getUserPrincipal().getName());
	}
	
	@DeleteMapping(Mappings.API_NOTES_SINGLE)
	public String deleteNote(@PathVariable long id) {
		noteService.deleteNote(id);
		return "{}";
	}
}
