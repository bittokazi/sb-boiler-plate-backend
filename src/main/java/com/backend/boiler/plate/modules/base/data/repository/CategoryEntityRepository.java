package com.backend.boiler.plate.modules.base.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.backend.boiler.plate.model.Category;

@Repository
public interface CategoryEntityRepository extends JpaRepository<Category, Long> {

}
