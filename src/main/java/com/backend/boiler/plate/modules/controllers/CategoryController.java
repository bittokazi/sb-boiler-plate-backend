package com.backend.boiler.plate.modules.controllers;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.boiler.plate.Utils.Mappings;
import com.backend.boiler.plate.model.Category;
import com.backend.boiler.plate.modules.services.CategoryService;

@RestController
@RequestMapping(Mappings.API)
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@PostMapping(Mappings.API_CATEGORY_ADD)
	public Category saveCategory(@RequestBody Category category, HttpServletRequest request) {
		return categoryService.saveCategory(category, request.getUserPrincipal().getName());
	}
	
	@GetMapping(Mappings.API_CATEGORY)
	public Set<Category> getCategories(HttpServletRequest request) {
		return categoryService.getCategories(request.getUserPrincipal().getName());
	}

}
