package com.backend.boiler.plate.modules.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.boiler.plate.Utils.Constants;
import com.backend.boiler.plate.model.MenuItem;
import com.backend.boiler.plate.model.Role;
import com.backend.boiler.plate.model.User;
import com.backend.boiler.plate.modules.base.data.repository.UserRepository;

@Service
public class UserAccessMenuGenerator {
	
	public List<MenuItem> generateMenu(Optional<User> useOptional) {
		List<MenuItem> menuItems = new ArrayList<MenuItem>();
		for(Role role: useOptional.get().getRoles()) {
			if(role.getName().equals("ROLE_"+Constants.getInstance().ROLE_ADMIN)) {
				MenuItem menuItem = new MenuItem();
				
				menuItem.setPath("/dashboard");
				menuItem.setTitle("Home");
				menuItem.setShow(true);
				menuItems.add(menuItem);
				
				menuItem = new MenuItem();
				menuItem.setPath("/dashboard/users");
				menuItem.setTitle("Users");
				menuItem.setShow(true);
				menuItems.add(menuItem);
				
				menuItem = new MenuItem();
				menuItem.setPath("/dashboard/users/add");
				menuItem.setTitle("Add User");
				menuItem.setShow(true);
				menuItems.add(menuItem);
				
				menuItem = new MenuItem();
				menuItem.setPath("/dashboard/users/:id");
				menuItem.setTitle("Update User");
				menuItem.setShow(false);
				menuItems.add(menuItem);
				
			} else if(role.getName().equals("ROLE_"+Constants.getInstance().ROLE_USER)) {
				
				MenuItem menuItem = new MenuItem();
				
				menuItem.setPath("/dashboard");
				menuItem.setTitle("Home");
				menuItem.setShow(true);
				menuItems.add(menuItem);
				
				menuItem = new MenuItem();
				menuItem.setPath("/dashboard/notes");
				menuItem.setTitle("Notes");
				menuItem.setShow(true);
				menuItems.add(menuItem);
				
				menuItem = new MenuItem();
				menuItem.setPath("/dashboard/notes/add");
				menuItem.setTitle("Add Note");
				menuItem.setShow(true);
				menuItems.add(menuItem);
				
				menuItem = new MenuItem();
				menuItem.setPath("/dashboard/notes/:id");
				menuItem.setTitle("Update Note");
				menuItem.setShow(false);
				menuItems.add(menuItem);
				
				menuItem = new MenuItem();
				menuItem.setPath("/dashboard/categories");
				menuItem.setTitle("Categories");
				menuItem.setShow(true);
				menuItems.add(menuItem);
				
				menuItem = new MenuItem();
				menuItem.setPath("/dashboard/categories/add");
				menuItem.setTitle("Add Category");
				menuItem.setShow(true);
				menuItems.add(menuItem);
			}
		}
		return menuItems;
	}
}
