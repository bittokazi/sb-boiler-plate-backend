package com.backend.boiler.plate.modules.base.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.backend.boiler.plate.model.Note;


public interface NoteEntityRepository extends JpaRepository<Note, Long> {

}
