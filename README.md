# sb-boiler-plate-backend
health URL: http://localhost:8080/sb-boiler-plate-backend/actuator/health

# Configure Database
Please set below Environment Variables to run the application:

```bash
DB_HOSTNAME
DB_NAME
DB_USERNAME
DB_PASSWORD
```

if you use eclipse/intellij you can add the environment variable from tomcat configuration page

If you are unable to set environment variable please set the database configuration in application.yml file

# Default admin login

```bash
username: admin
password: password
```
